import React from "react";
import HomeView from "../../components/pages/home";

export default function HomePage() {
  return <HomeView />;
}
