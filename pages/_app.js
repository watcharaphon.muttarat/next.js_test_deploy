import styles from "../styles/globals.css";
import "antd/dist/antd.css";
function MyApp({ Component, pageProps }) {
  return <Component styles={styles} {...pageProps} />;
}

export default MyApp;
