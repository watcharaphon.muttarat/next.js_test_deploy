import { useLayoutEffect, useState } from "react";

const useWindowsScroll = () => {
  const [scrollY, setScrollY] = useState(0);

  useLayoutEffect(() => {
    const handleScroll = () => {
      setScrollY(window.scrollY);
    };
    handleScroll();

    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return scrollY;
};

export default useWindowsScroll;

// How To Use
// const scroll = useWindowsScroll();
