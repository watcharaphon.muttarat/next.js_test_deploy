import React from "react";
import MainLayout from "../../templates/main-layout";
import MainLayoutSecond from "../../templates/main-layout-second";

function DashboardView() {
  return <MainLayoutSecond>DashboardView</MainLayoutSecond>;
}

export default DashboardView;
