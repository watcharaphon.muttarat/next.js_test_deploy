import React from "react";
import MainLayoutSecond from "../../templates/main-layout-second";
import Image from "next/image";
import { Grid } from "@mui/material";

function HomeView() {
  return (
    <MainLayoutSecond>
      <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="center"
      >
        <div>
          <Image
            src="/img/inet_home.png"
            width={550}
            height={300}
            alt="oneportal"
          />
        </div>
      </Grid>
    </MainLayoutSecond>
  );
}

export default HomeView;
