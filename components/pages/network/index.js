import React from "react";
import Button from "../../atoms/button";
import CardContent from "../../atoms/card-content";
import InputText from "../../atoms/input-text";
import LoaderItem from "../../atoms/loader-item";
import SelectDate from "../../atoms/select-date";
import SelectItem from "../../atoms/select-item";
import TableDataStandard from "../../atoms/table-data-standard";
import SearchItem from "../../molecules/search-item";
import MainLayout from "../../templates/main-layout";

function NetworkView() {
  const dataSource = [
    {
      key: "1",
      name: "Mike",
      age: 32,
      address: "10 Downing Street",
    },
    {
      key: "2",
      name: "John",
      age: 42,
      address: "10 Downing Street",
    },
  ];

  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Age",
      dataIndex: "age",
      key: "age",
    },
    {
      title: "Address",
      dataIndex: "address",
      key: "address",
    },
  ];
  return (
    <MainLayout>
      NetworkView
      <CardContent>
        <Button type="primary" label="Primary"></Button>
        <SelectItem></SelectItem>
        <SelectDate />
        <InputText width={100} />
        <SearchItem />
        <LoaderItem />
        <br></br>
        <br></br>
        <TableDataStandard dataSource={dataSource} columns={columns} />
      </CardContent>
    </MainLayout>
  );
}

export default NetworkView;
