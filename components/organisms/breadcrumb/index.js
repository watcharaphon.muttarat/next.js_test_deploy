import { Breadcrumbs as MUIBreadcrumbs } from "@material-ui/core";
import { withRouter } from "next/router";
import Link from "next/link";
const Breadcrumb = ({ router }) => {
  const pathNames = router.pathname.split("/").filter((x) => x);
  return (
    <MUIBreadcrumbs>
      {pathNames.map((name, index) => {
        const nameSplit = name.split("_").join(" ");
        const routeTo = `/${pathNames.slice(0, index + 1).join("/")}`;
        const isLast = index === pathNames.length - 1;
        return isLast ? (
          <div key={name} style={{ color: "#0C729F", fontWeight: 700 }}>
            {nameSplit.charAt(0).toUpperCase() + nameSplit.slice(1)}
          </div>
        ) : (
          <Link
            key={name}
            onClick={() => history.push(routeTo)}
            style={{ color: "#0C729F", fontWeight: 400 }}
          >
            {nameSplit.charAt(0).toUpperCase() + nameSplit.slice(1)}
          </Link>
        );
      })}
    </MUIBreadcrumbs>
  );
};

export default withRouter(Breadcrumb);
