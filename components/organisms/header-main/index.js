import { useState } from "react";
import { Grid } from "@material-ui/core";
import styles from "./styles.module.css";
import Image from "next/image";
import Button from "../../atoms/button";
import useWindowsScroll from "../../../helper/hook/windows-scroll";
import ModalItem from "../../atoms/modal-item";

function HeaderMain() {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const scroll = useWindowsScroll();
  return (
    <>
      <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="center"
        className={
          scroll > 0
            ? styles.headerMainLayoutSecondScroll
            : styles.headerMainLayoutSecond
        }
      >
        <Grid
          container
          direction="row"
          justifyContent="center"
          alignItems="center"
        >
          <Grid
            container
            direction="row"
            justifyContent="flex-start"
            alignItems="center"
            item
            xs={6}
            className={styles.gridItemLogo}
          >
            <Image
              src="/img/logo.png"
              width="133px"
              height="40px"
              alt="oneportal"
            />
          </Grid>
          <Grid
            container
            direction="row"
            justifyContent="flex-end"
            alignItems="center"
            item
            xs={6}
            className={styles.gridItemButton}
          >
            <Button
              type="secondary"
              label="Sign In"
              width="85px"
              height="30px"
              onClick={() => {
                setIsModalVisible(!isModalVisible);
              }}
            />
          </Grid>
        </Grid>
      </Grid>
      <ModalItem
        visible={isModalVisible}
        setIsModalVisible={setIsModalVisible}
      ></ModalItem>
    </>
  );
}

export default HeaderMain;
