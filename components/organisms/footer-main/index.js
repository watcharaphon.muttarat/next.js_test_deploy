import { Grid } from "@mui/material";
import Image from "next/image";
import styles from "./styles.module.css";
function FooterMain() {
  return (
    <div className={styles.main}>
      <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="flex-start"
      >
        <Grid
          container
          direction="row"
          justifyContent="center"
          alignItems="center"
          item
          xs={5}
        >
          <Image src="/img/logo.png" width={230} height={70} alt="oneportal" />
        </Grid>
        <Grid
          container
          direction="row"
          justifyContent="flex-start"
          alignItems="center"
          item
          xs={7}
        >
          <Grid
            container
            direction="row"
            justifyContent="flex-start"
            alignItems="center"
          >
            <Grid item xs={12} className={styles.txtMainAddress}>
              Address
            </Grid>
            <Grid className={styles.txtDetailAddress}>
              Internet Thailand Public Company Limited 1768 Thai Summit Tower,
              10 th 12 th Floor and IT Floor New Petchaburi Road, Khwaeng
              BangKapi, Khet Huay Khwang, Bangkok 10310
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="center"
      >
        <Grid
          container
          direction="row"
          justifyContent="center"
          alignItems="center"
          item
          xs={12}
        >
          <Grid className={styles.txtCompany}>
            Privacy Policy | Term of Service © Copyright 2020, Internet Thailand
            Public Company Limited.
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
}

export default FooterMain;
