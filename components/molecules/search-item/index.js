import React from "react";
import IconSearch from "../../atoms/icons/icon-search";
import InputText from "../../atoms/input-text";

function SearchItem() {
  return (
    <InputText
      placeholder="ค้นหา.."
      suffix={<IconSearch color="#DDDDDD" />}
      width={100}
    />
  );
}

export default SearchItem;
