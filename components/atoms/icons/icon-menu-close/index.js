import React from "react";
import { MenuFoldOutlined } from "@ant-design/icons";
function IconMenuClose({ color, fontSize }) {
  return <MenuFoldOutlined style={{ color: color, fontSize: fontSize }} />;
}

export default IconMenuClose;

//color = '#f56'
