import React from "react";
import { SearchOutlined } from "@ant-design/icons";
function IconSearch({ color }) {
  return <SearchOutlined style={{ color: color }} />;
}

export default IconSearch;

//color = '#f56'
