import React from "react";
import { MenuUnfoldOutlined } from "@ant-design/icons";
function IconMenuOpen({ color, fontSize }) {
  return <MenuUnfoldOutlined style={{ color: color, fontSize: fontSize }} />;
}

export default IconMenuOpen;

//color = '#f56'
//fontSize = '10px'
