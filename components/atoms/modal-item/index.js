import React from "react";
import { Modal } from "antd";
function ModalItem({ children, visible, setIsModalVisible }) {
  const handleCancel = () => {
    setIsModalVisible(false);
  };
  return (
    <>
      <Modal visible={visible} footer={null} onCancel={handleCancel}>
        {children}
      </Modal>
    </>
  );
}

export default ModalItem;

// visible = {true} or {false} (State)
// setIsModalVisible = {true} or {false}  (setState)
