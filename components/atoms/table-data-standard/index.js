import React from "react";
import { Table } from "antd";
function TableDataStandard({ dataSource, columns }) {
  return <Table dataSource={dataSource} columns={columns} />;
}

export default TableDataStandard;
