import React from "react";
import { Input } from "antd";
function InputText({ placeholder, width, suffix, onChange }) {
  return (
    <Input
      suffix={suffix}
      style={{
        width: width,
        borderRadius: 8,
      }}
      placeholder={placeholder}
      onChange={onChange}
    />
  );
}

export default InputText;

//placeholder = 'text'
//width = '100px', 100
//suffix คือ icon = <icon />
