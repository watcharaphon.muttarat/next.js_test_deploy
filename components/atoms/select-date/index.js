import React from "react";
import { DatePicker } from "antd";

export default function SelectDate({ onChange, picker }) {
  return <DatePicker onChange={onChange} picker={picker} />;
}

// หน้าตาฟังค์ชั่น onChange
// const onChange = (date, dateString) => {
//     console.log(date, dateString);
//   };

//picker = week, month, quarter, year
//ถ้าไม่ส่ง picker จะเป็น = ปี/เดือน/วัน
