import styles from "./styles.module.css";
export default function Button({
  type,
  label,
  disabled,
  icon,
  onClick,
  width,
  height,
}) {
  const styleBtn = () => {
    if (type === "primary") {
      return styles.primary;
    } else if (type === "secondary") {
      return styles.secondary;
    } else {
      return null;
    }
  };

  return (
    <button
      disabled={disabled ? true : false}
      className={styleBtn()}
      onClick={onClick}
      style={{
        width: width ? width : "auto",
        height: height ? height : "auto",
      }}
    >
      {icon ? (
        <div className={styles.gridContainer}>
          <div className={styles.gridItem}>{icon}</div>
          <div className={styles.gridItem2}>{label}</div>
        </div>
      ) : (
        <div>{label}</div>
      )}
    </button>
  );
}

//type = 'primary' , 'secondary'
//label = 'text'
//disabled default = false
//icon = <icon />
//onClick = function
