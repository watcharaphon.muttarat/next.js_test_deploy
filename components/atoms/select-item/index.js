import React from "react";
import { Select } from "antd";
function SelectItem({ onChange }) {
  const { Option } = Select;
  return (
    <Select
      defaultValue="Day"
      dropdownStyle={{ borderRadius: 10 }}
      onChange={onChange}
    >
      <Option value="Day">Day</Option>
      <Option value="Month">Month</Option>
      <Option value="Quarter">Quarter</Option>
    </Select>
  );
}

export default SelectItem;

//หน้าตาฟังค์ชั่น onChange
// const handleChange = (value) => {
//   console.log(`selected ${value}`);
// };
