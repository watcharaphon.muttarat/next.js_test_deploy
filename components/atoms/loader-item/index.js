import React from "react";
import { Spin } from "antd";
function LoaderItem({ tip, size }) {
  return <Spin tip={tip} size={size} />;
}

export default LoaderItem;

//size = 'small', 'middle', 'large'
//tip คือ ข้อความที่จะแสดงเพิ่มเติม = 'loading...'
