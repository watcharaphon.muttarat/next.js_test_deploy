import React from "react";
import { Card } from "antd";

function CardContent({ children, width, height }) {
  return (
    <Card
      style={{
        borderRadius: 10,
        background: "#ffffff",
        width: width,
        height: height,
      }}
    >
      {children}
    </Card>
  );
}

export default CardContent;
