import styles from "./styles.module.css";
// import useWindowSize from "../../../helper/hook/windows-size";
// import useWindowsScroll from "../../../helper/hook/windows-scroll";
import HeaderMain from "../../organisms/header-main";
import FooterMain from "../../organisms/footer-main";

function MainLayoutSecond({ children }) {
  //   const size = useWindowSize();
  // const scroll = useWindowsScroll();
  return (
    <div className={styles.main}>
      <HeaderMain />
      <div className={styles.children}>{children}</div>;
      <FooterMain />
    </div>
  );
}

export default MainLayoutSecond;
